# Imagen para despliegues de aplicaciónes realizadas en Angular

### Aplicaciónes instaladas

 - Nginx
 - NodeJS
 - Angular CLI

### Configuración par adespliegue

#### Dockerfile

 Reemplazar las siguientes variables dentro del Dockerfile indicando URI y nombre del repositorio
 
 - ENV GIT_REPOSITORIO=https://elcascarudo@bitbucket.org/elcascarudo/despliegue-angular-node.git
 - ENV GIT_PROYECTO=despliegue-angular-node 
 
```
FROM elcascarudodev/nginx-nodejs-angular

ENV GIT_REPOSITORIO=https://elcascarudo@bitbucket.org/elcascarudo/despliegue-angular-node.git
ENV GIT_PROYECTO=despliegue-angular-node

WORKDIR /var/www/html
RUN rm -f index.nginx-debian.html
RUN git clone $GIT_REPOSITORIO

WORKDIR /var/www/html/$GIT_PROYECTO
RUN npm install
RUN ng build --prod
RUN cp dist/$GIT_PROYECTO/* /var/www/html/

WORKDIR /var/www/html
RUN rm -rf $GIT_PROYECTO

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
``` 

#### docker-compose

```
version: '3'

services:
  frontend:
    container_name: ejemplo-angular
    build: .
    restart: always
    ports:
      - '8099:80'
```

#### Ejecución

```
docker-compose up --build -d
```
