FROM elcascarudodev/nginx-nodejs-angular

ENV GIT_REPOSITORIO=https://elcascarudo@bitbucket.org/elcascarudo/despliegue-angular-node.git
ENV GIT_PROYECTO=despliegue-angular-node

WORKDIR /var/www/html

RUN rm -f index.nginx-debian.html

RUN git clone $GIT_REPOSITORIO

WORKDIR /var/www/html/$GIT_PROYECTO
RUN npm install
RUN ng build --prod

RUN cp dist/$GIT_PROYECTO/* /var/www/html/

WORKDIR /var/www/html
RUN rm -rf $GIT_PROYECTO

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]